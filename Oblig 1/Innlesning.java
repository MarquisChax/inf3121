import java.util.Scanner;
import java.io.File;

/**
*Leser inn et brett fra en gitt fil.
*/
public class Innlesning {

	private String filnavn;
	private Sudokubeholder sudokubeholder;
	private Brett brett = null;
	private Kolonne[] kolonne = null;
	private Rad rad = null;
	private Boks boks = null;
	private boolean skrivTilSkjerm;
	private int antallRader = 0;
	private int antallKolonner = 0;

	/**
	*@param fil Filnavn fra bruker lagres i en egen String.
	*/
	Innlesning(String fil) {
		filnavn = fil;
		sudokubeholder = new Sudokubeholder();
		skrivTilSkjerm = true;
		lesFraFil(skrivTilSkjerm);
	}

	/**
	*@return Returnerer Sudokubeholder.
	*/
	public Sudokubeholder getSudokubeholder() {
		return sudokubeholder;
	}

	/**
	*Ganger sammen antall rader og antall kolonner. For å få korrekt brettstørrelse må
	*tallet ganges med seg selv en gang til utenfor metoden.
	*@return Returnerer størrelsen på brettet. 
	*/
	public int getBrettstorrelse() {
		return antallRader*antallKolonner;
	}

	/**
	*@return Returnerer antall rader i bokser.
	*/
	public int getAntallRader() {
		return antallRader;
	}

	/**
	*@return Returnerer antall kolonner i en boks.
	*/
	public int getAntallKolonner() {
		return antallKolonner;
	}

	/**
	*Leser inn et brett fra fil ved å bruke det gitte filnavnet. 
	*/
	public void lesFraFil(boolean skrivTilSkjerm) {
		Scanner fil = null;
		try{
			fil = new Scanner(new File(filnavn));	
		}catch(Exception e){
			e.printStackTrace();
		}

		nyttBrett(fil, skrivTilSkjerm);
		fil.close(); 
	}

	/**
	*Lager et nytt brett med tilhørende innmat.
	*@param scanner Tar imot et objekt av klassen Scanner, som deretter brukes til å lese fra fil. 
	*/
	public void nyttBrett(Scanner scanner, boolean skrivTilSkjerm) {
		Scanner fil = scanner;
		String linje = "";
		int innlestLinje = 0;

		while(fil.hasNext()) {
			linje = fil.nextLine();

			//Her har programmet fått inn EN RAD 
			if(linje.length() == 1) { 

				//Dette henter ut lengde og bredde på brettet, de to første linjene i filen.
				if(antallRader == 0) {
					antallRader = Integer.parseInt(linje);
				} else {
					antallKolonner = Integer.parseInt(linje);
					brett = new Brett(antallRader, antallKolonner, sudokubeholder);
				}
			} else {
				kolonne = new Kolonne[antallRader*antallKolonner];

				for(int i = 0; i < linje.length(); i++) {
					/**
					*Den eneste gangen det skal lages kolonner, er den første gangen en rad blir 
					*lest inn. Etterpå får rutene bare pekere til kolonner på plass i. 
					*/
					if(kolonne[kolonne.length-1] == null) {
						Kolonne k = new Kolonne(antallRader, antallKolonner);
						kolonne[i] = k;
						brett.lagreKolonne(k);
					}
					
					//Hver gang for-løkka begynner på nytt, lages det en ny rad som lagres i brettet.
					if(i == 0) {
						rad = new Rad(antallRader, antallKolonner);
						brett.lagreRad(rad);
					}
				}

				opprettBokser(linje, antallRader, antallKolonner, innlestLinje);
				innlestLinje++;
			}	
		}
		
		brett.settRuteNestepekere();

		if(skrivTilSkjerm) {
			utskriftTilSkjerm();
		}
	}

	/**
	*Oppretter bokser.
	*@param linje Er den innleste linja.
	*@param antallRader Angir antall rader.
	*@param antallKolonner Angir antall kolonner.
	*@param innlestLinje Angir hvor mange linjer er lest inn hittil.
	*/
	public void opprettBokser(String linje, int antallRader, int antallKolonner, int innlestLinje) {
		
		for(int i = innlestLinje; i < linje.length(); i++) {
			for(int j = 0; j < linje.length(); j++) {
				
				/*Hver gang denne utregningen gir 0 på begge, lages det en ny boks.
				 *Her er vi på den første ruta i en ny boks.*/
				if(i % antallRader == 0 && j % antallKolonner == 0) {
					boks = new Boks(antallRader, antallKolonner);
					brett.lagreBoks(boks);
					opprettRute(linje.charAt(j), j, boks); 

				} else if(i % antallRader == 0 && j % antallKolonner != 0) {
					Rute r = brett.getRute(i, j-1);
					boks = r.getBoks(); 
					opprettRute(linje.charAt(j), j, boks);

				} else if(i % antallRader != 0 && j % antallKolonner == 0) {
					Rute r = brett.getRute(i-1, j);
					boks = r.getBoks();
					opprettRute(linje.charAt(j), j, boks); 

				} else if(i % antallRader != 0 && j % antallKolonner != 0) {
					Rute r = brett.getRute(i, j-1);
					boks = r.getBoks();
					opprettRute(linje.charAt(j), j, boks);
				}
			}   break;
		}  
	}

	/**
	*Oppretter ruter.
	*@param c Er en char fra den innleste linja.
	*@param i Angir hvilken kolonne ruta hører til.
	*@parm b Er et objekt av klassen Boks.
	*/
	public void opprettRute(char c, int i, Boks b) {
		if(c == '.') { //Dersom det er et punktum, er det en tom rute.
			RuteSomTrengerVerdi rute = new RuteSomTrengerVerdi(b, brett.getKolonne(i), rad, brett);
								
			brett.settInnRute(rute);
			rad.lagreRute(rute);
			b.lagreRute(rute);
			brett.lagreRuteIKolonnebeholder(rute, i);

		} else { //Ellers er det en rute som har et tall.
			int tall = Character.getNumericValue(c);

			if(tall <= 9) {
			} else {
				tall = sudokubeholder.konverterBokstavTilTall(c);
			}
			
			RuteMedVerdi rute = new RuteMedVerdi(tall, b, brett.getKolonne(i), rad, brett);
								
			brett.settInnRute(rute);
			rad.lagreRute(rute);
			b.lagreRute(rute);
			brett.lagreRuteIKolonnebeholder(rute, i);
		}
	}

	/**
	*Kaller på en utskriftsmetode i Sudokubeholder.
	*/
	public void utskriftTilSkjerm() {
		sudokubeholder.skrivUtLosningsforslag();
	}
}
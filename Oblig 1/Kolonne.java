/**
*Denne klassen modellerer en sudoku-kolonne.
*/
public class Kolonne extends Brettdeler {

	/**
	*Konstruktøren tar imot tall som ganges med hverandre, og sendes videre til superklassens konstruktør.
	*@param x Er et heltall lest inn fra fil (antall rader).
	*@param y Er et heltall lest inn fra fil (antall kolonner).
	*/
	Kolonne(int x, int y) {
		super(x*y);
	}
}
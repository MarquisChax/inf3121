import java.util.ArrayList;

/**
*Denne klassen tar vare på løsningsforslag.
*/
class Sudokubeholder {

	private ArrayList <String [][]> brettbeholder = new ArrayList <String [][]>(750);
	private int antallLosninger = 0;

	/**
	*Setter inn gitt brett på en ledig plass i beholderen, og lagrer
	*antall losninger uansett om det er plass i beholderen eller ei.
	*@param brett Tar imot et gitt objekt av klassen Brett.
	*/
	public void settInn(String[][] losningsforslag) {

		if(brettbeholder.size() < 750) {
			brettbeholder.add(losningsforslag); 
		} 
		antallLosninger++; //Beholderen er full, men antall mulige løsninger blir lagret.
		System.out.println("Antall løsninger: " + antallLosninger);
	} 

	/**
	*Henter et løsningsforslag fra arraylisten basert på indeks.
	*@param index Er et heltall som brukes til å finne et løsningsforslag i arraylisten.
	*@return Returnerer en todimensjonal array med et løsningsforslag.
	*/
	public String[][] getLosningsforslagFraIndeks(int index) {
			return brettbeholder.get(index);
	}

	public void nullstillBrettbeholder() {
		for(String[][] s : brettbeholder) {
			if(s != null) {
				s = null;
			}
		}
	}

	/**
	*@return Returnerer størrelsen til brettbeholderen.
	*/
	public int getBrettbeholderSize() {
		return brettbeholder.size();
	}

	/**
	*Skriver ut innholdet til et brett gjennom å kalle på en metode i objektet.
	*/
	public void skrivUtLosningsforslag() {
		int teller = 1;
		for(String[][] s : brettbeholder) {
			if(s != null) {
				System.out.println(teller + ": ");
				hentLosningsforslag(s); teller++;
			}
		}
	}

	/**
	*Skriver ut alle tallene i ett løsningsforslag til skjerm i et sudokuformat.
	*@param s Er et todimensjonalt String-array. 
	*/
	public void hentLosningsforslag(String[][] s) {
		for(int i = 0; i < s.length; i++) {
			for(int j = 0; j < s.length; j++) {
				System.out.print(s[i][j] + " ");
			}

			System.out.println("");
		}
	}

	/**
	*Skriver ut alle tilgjengelige løsninger i et format tilpasset fil.
	*@param s Er et brett i Stringformat gjort om fra originalbrettet.
	*@return Returnerer hele brettet i en String.
	*/
	public String hentLosningsforslagIFilformat(String[][] s) {
		String temp = "";
		for(int i = 0; i < s.length; i++) {
			for(int j = 0; j < s.length; j++) {
				temp += (s[i][j]);
			}
			temp += "//";
		}
		return temp;
	}

	/**
	*Skriver hvor mange løsninger det ble funnet totalt.
	*@return Returnerer en String med antall løsninger.
	*/
	public String skrivAntallLosningerTilFil() {
		String antall = "Antall mulige løsninger: " + antallLosninger;
		return antall;
	}

	/**
	*Setter sammen alle funnede og lagrede løsninger til én stor String.
	*@return Returnerer en String med alle lagrede løsninger.
	*/
	public String skrivTilFil() {
		String fraBrett = "";
		for(String[][] s : brettbeholder) {
			if(s != null) {
				fraBrett += hentLosningsforslagIFilformat(s) + "\n";
			}
		}

		return fraBrett;
	}

	/**
	*@return Returnerer et heltall med antall mulige løsninger et brett kan løses med.
	*/
	public int getAntallLosninger() {
		return antallLosninger;
	}

	/**
	*Tall som er større enn ni sendes til den gigantiske switch-saken, som returnerer en String med en bokstav,
	*fordi løsningsforslaget tar kun imot String, ikke char.
	*@param tall Er et heltall sendt fra Rutebeholder i Brett.
	*@return Returnerer en String. 
	*/
	public String konverterTallTilBokstav(int tall) {

		switch(tall) {
			case 10: return "A";
			case 11: return "B";
			case 12: return "C";
			case 13: return "D";
			case 14: return "E";
			case 15: return "F";
			case 16: return "G";
			case 17: return "H";
			case 18: return "I";
			case 19: return "J";
			case 20: return "K";
			case 21: return "L";
			case 22: return "M";
			case 23: return "N";
			case 24: return "O";
			case 25: return "P";
			case 26: return "Q";
			case 27: return "R";
			case 28: return "S";
			case 29: return "T";
			case 30: return "U";
			case 31: return "V";
			case 32: return "W";
			case 33: return "X";
			case 34: return "Y";
			case 35: return "Z";
			case 36: return "@";
			default: return "!!";
		}
	}

	/**
	*Denne metoden brukes under innlesning av brett som er større enn 9x9, for å konvertere bokstaver til tall.
	*@param bokstav Er en char sendt fra Innlesning, under opprettelse av rute.
	*@return Returnerer et tall. 
	*/
	public int konverterBokstavTilTall(char bokstav) {

		switch(bokstav) {
			case 'A': return 10;
			case 'B': return 11;
			case 'C': return 12;
			case 'D': return 13;
			case 'E': return 14;
			case 'F': return 15;
			case 'G': return 16;
			case 'H': return 17;
			case 'I': return 18;
			case 'J': return 19;
			case 'K': return 20;
			case 'L': return 21;
			case 'M': return 22;
			case 'N': return 23;
			case 'O': return 24;
			case 'P': return 25;
			case 'Q': return 26;
			case 'R': return 27;
			case 'S': return 28;
			case 'T': return 29;
			case 'U': return 30;
			case 'V': return 31;
			case 'W': return 32;
			case 'X': return 33;
			case 'Y': return 34;
			case 'Z': return 35;
			case '@': return 36;
			default: return 0;
		}
	}
}
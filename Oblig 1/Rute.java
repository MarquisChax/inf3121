 import java.util.ArrayList;

/**
*Denne klassen inneholder alt det en rute skal kunne om seg selv, samt metoder for å fylle ut en verdi til tomme ruter.
*/
 abstract class Rute {
	protected int verdi;
	public Rute neste;
	protected Boks boks;
	protected Kolonne kolonne;
	protected Rad rad;
	protected Brett brett;
	protected ArrayList <Integer> verdibeholder;

	/**
	*Konstruktøren initialiserer de forskjellige variablene med pekere til objekter i Brett. 
	*@param boks Tar imot en peker til Boks.
	*@param kolonne Tar imot en peker til Kolonne.
	*@param rad Tar imot en peker til Rad.
	*@param brett Tar imot en peker til Brett. 
	*/
	Rute(Boks boks, Kolonne kolonne, Rad rad, Brett brett) {
		neste = null;
		this.boks = boks;
		this.kolonne = kolonne;
		this.rad = rad;
		this.brett = brett;
		verdibeholder = new ArrayList<Integer>(); 
	}

	/**
	*Fyller ut hele brettet. Rekursiv metode. 
	*/
	abstract public void fyllUtRestenAvBrettet();

	/**
	*@return Returnerer et heltall, den nåværende verdien til ruta.
	*/
	public int getVerdi() {
		return verdi;
	}

	/**
	*@return Returnerer rutens kolonneobjekt.
	*/
	public Kolonne getKolonne() {
		return kolonne;
	}

	/**
	*@return Returnerer rutens radobjekt.
	*/
	public Rad getRad() {
		return rad;
	}

	/**
	*@return Returnerer rutens boksbjekt.
	*/
	public Boks getBoks() {
		return boks;
	}

	/**
	*@return Returnerer rutens brettobjekt.
	*/
	public Brett getBrett() {
		return brett;
	}

	/**
	*En rute skal kunne lagres i en kolonne.
	*@param rute Tar imot et objekt av klassen Rute.
	*/
	public void lagreRuteIKolonne() {
		kolonne.lagreRute(this);
	}

	/**
	*En rute skal kunne lagres i en rad.
	*@param rute Tar imot et objekt av klassen Rute.
	*/
	public void lagreRuteIRad() {
		rad.lagreRute(this);
	}

	/**
	*En rute skal kunne lagres i en boks.
	*@param rute Tar imot et objekt av klassen Rute.
	*/
	public void lagreRuteIBoks() {
		boks.lagreRute(this);
	}
}

/**
*Denne klassen er for ruter som allerede har en verdi når de blir lest fra fil.
*/
class RuteMedVerdi extends Rute {

	/**
	*Konstruktøren sender alle parametere bortsett fra verdi opp til superklassens konstruktør.
	*Setter variabelen verdi til det tallet som ble lest inn fra fil.
	*@param verdi Tar imot et heltall fra fil.
	*@param boks Tar imot en peker til Boks.
	*@param kolonne Tar imot en peker til Kolonne.
	*@param rad Tar imot en peker til Rad.
	*@param brett Tar imot en peker til Brett. 
	*/
	RuteMedVerdi(int verdi, Boks boks, Kolonne kolonne, Rad rad, Brett brett) {
		super(boks, kolonne, rad, brett);
		this.verdi = verdi;
	}

	/**
	*Sender programmet videre til neste rute, ettersom denne ruta har en verdi fra før.
	*/
	public void fyllUtRestenAvBrettet() {
		if(neste != null) {
			//System.out.println("Hadde tall: " + verdi);
			neste.fyllUtRestenAvBrettet();
		} else {
			brett.konverterBrettTilLosningsforslag();
		}
	}
}

/**
*Denne klassen er for ruter som ikke har en verdi når de blir lest fra fil. 
*/
class RuteSomTrengerVerdi extends Rute {

	/**
	*Konstruktøren sender alle parametere bortsett fra verdi opp til superklassens konstruktør.
	*Setter variabelen verdi til 0.
	*@param boks Tar imot en peker til Boks.
	*@param kolonne Tar imot en peker til Kolonne.
	*@param rad Tar imot en peker til Rad.
	*@param brett Tar imot en peker til Brett. 
	*/
	RuteSomTrengerVerdi(Boks boks, Kolonne kolonne, Rad rad, Brett brett) {
		super(boks, kolonne, rad, brett);
		verdi = 0;
	}

	/**
	*Finner en mulig verdi for denne ruten og hopper videre til neste rute.
	*/
	public void fyllUtRestenAvBrettet() {
		for(int i = 0; i < kolonne.getRuteoversiktLength(); i++) {
			if(boks.finnesTallet(i+1) == false) {
				if(kolonne.finnesTallet(i+1) == false) {
					if(rad.finnesTallet(i+1) == false) {

							//Dersom alle FIRE testene passerer, er i en gyldig verdi og verdien blir lagret i rutens variabel.
							verdi = i+1;
							//System.out.println("Fant tall: " + verdi);
							//På dette tidspunktet har verdi blitt oppdatert, noe som vil si at da vet brettet at ruta har en verdi.

							if(neste != null) {
								neste.fyllUtRestenAvBrettet(); //Kaller på samme metode i neste.
							} else {
								//Dersom neste er null, er vi på slutten av brettet, og det må konverteres til et løsningsforslag.
								brett.konverterBrettTilLosningsforslag();
							}
							
							verdi = 0;
					}
				} 
			}
		} //end for-løkke
	} //end fyllUtRestenAvBrettet()
}
/**
*Denne klassen holder styr på brettdeler samt hvilke tall som finnes i dem. Jeg innser at den
*kunne vært abstract istedenfor å implementere et grensesnitt, men ettersom jeg allerede har
*en abstract klasse (Rute), ønsket jeg å vise frem bruk av grensesnitt.
*/
public class Brettdeler implements Bruteforce {

	protected Rute[] ruteoversikt;

	/**
	*I konstruktøren lages det en ruteoversikt med tall tilsendt fra konstruktør til subklasse.
	*@param tall Er et heltall som bestemmer lengden på ruteoversikt-array.
	*/
	Brettdeler(int tall) {
		ruteoversikt = new Rute[tall];
	}

	/**
	*Skriver ut arrayet Ruteoversikt. 
	*/
	public void skrivUtRuteoversikt() {
		for(int i = 0; i < ruteoversikt.length; i++) {
			if(ruteoversikt[i] == null) continue;
			System.out.print(ruteoversikt[i].getVerdi() + "");
		}
		System.out.println("");
	}

	/**
	*Finner ut om tallet finnes fra før av. 
	*1. Boks: Tall som finnes i boksen fra før av, avgjør hvilke tall som mangler.
	*2. Kolonne: Tall som finnes i kolonnen fra før av, avgjør hvilke tall som kan 
	*plasseres i ruter som inngår i denne kolonnen.
	*3. Rad: Tall som finnes i raden fra før av, avgjør hvilke tall som kan plasseres
	*i ruter som inngår i denne raden. 
	*Dersom den ikke finner to like tall, lagres tallet i ruteoversikten. 
	*@param rute tar imot et objekt av klassen Rute.
	*@return Returnerer true dersom den finner to ruter med like verdier.
	*/
	public boolean finnesTallet(int verdi) {
		for(Rute r : ruteoversikt) {
			if(r.getVerdi() == verdi) { //Dersom tallet finnes fra før, returneres true.
				return true;
			}
		} return false; 
	}

	/**
	*@return Returnerer lengden til ruteoversikt-array.
	*/
	public int getRuteoversiktLength() {
		return ruteoversikt.length;
	}

	/**
	*Lagrer et objekt av klassen Rute i ruteoversikten.
	*@param rute Tar imot et objekt av klassen Rute.
	*/
	public void lagreRute(Rute rute) {
		for(int i = 0; i < ruteoversikt.length; i++) {
			if(ruteoversikt[i] == null) {
				ruteoversikt[i] = rute;  break;
			}
		}
	}
}
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
*Denne klassen er ansvarlig for GUI-delen av programmet.
*/
public class SudokuGUI extends JFrame {

	private Innlesning innlesning;
	private Sudokubeholder sudokubeholder;
	private JPanel panel;
	private JPanel panelMeny;
	private JButton neste;
	private JButton velgFil;
	private JLabel losninger;
	private JLabel viserLosning;
	private Lytter lytter;
	private int topp;
	private int venstre;
	private int hoyre;
	private int ned;
	private int losningsteller;
	private JTextArea [][] ruter;


	/**
	*Konstruktør. 
	*@param i Er et objekt av Innlesning.
	*/
	SudokuGUI(Innlesning i) {
		losningsteller = 0;
		innlesning = i;

		setTitle("INF1010 Obligatorisk oppgave 5");
		setSize(600, 600);

		lytter = new Lytter();
		panel = new JPanel();
		panelMeny = new JPanel();

		opprettInnmat();
	}

	/**
	*Lar bruker velge fil.
	*/	
	public void filvelger() {
		JFileChooser finnFil = new JFileChooser();
		String temp = "";

		int ting = finnFil.showOpenDialog(this);
		if(ting == JFileChooser.APPROVE_OPTION) {
			temp = finnFil.getSelectedFile().getName();
			innlesning = new Innlesning(temp);
			opprettInnmat();
		}
	}

	/**
	*Oppretter ting GUIet trenger for å fungere.
	*/
	public void opprettInnmat() {
		sudokubeholder = innlesning.getSudokubeholder();
		
		panel.setLayout(new GridLayout(innlesning.getBrettstorrelse(), innlesning.getBrettstorrelse()));
		ruter = new JTextArea[innlesning.getBrettstorrelse()][innlesning.getBrettstorrelse()];
		
		neste = new JButton("Neste");
		velgFil = new JButton("Velg fil");
		//losninger = new JLabel(sudokubeholder.skrivAntallLosningerTilFil());
		
		
		panelMeny.add(velgFil);
		panelMeny.add(neste);

		neste.addActionListener(lytter);
		velgFil.addActionListener(lytter); 

		hentLosningsforslag();
		panelMeny.add(viserLosning);
		add(panelMeny, BorderLayout.NORTH);

		panel.revalidate();
		panel.repaint();
		add(panel, BorderLayout.CENTER);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	/**
	*Henter et løsningsforslag fra sudokubeholderen.
	*/
	public void hentLosningsforslag() {
		String[][] temp = sudokubeholder.getLosningsforslagFraIndeks(losningsteller);
		if(temp != null) {
			lagRuter(temp);
			losningsteller++;
		} 
	}

	/**
	*Lager alle rutene med verdier.
	*@param losningsforslag Er et todimensjonalt String-array.
	*/
	public void lagRuter(String[][] losningsforslag) {
		topp = 1;
		venstre = 1;
		hoyre = 1;
		ned = 1;

		int antallRader = innlesning.getAntallRader();
		int antallKolonner = innlesning.getAntallKolonner();
		//Color farge = new Color(194, 224, 225);

		viserLosning = new JLabel("          Viser " + (losningsteller + 1) + " ut av " + sudokubeholder.getBrettbeholderSize());
		panelMeny.add(viserLosning);

		for(int i = 0; i < losningsforslag.length; i++) {
			topp = 1;

			//Forsøk på å lage tykke streker
			if(i % antallRader == 0 && i > 0) {
				topp = 3;
			}

			for(int j = 0; j < losningsforslag.length; j++) {
				venstre = 1;

				//Forsøk på å lage tykke streker
				if(j % antallKolonner == 0 && j > 0) {
				venstre = 3;
				}

				ruter[i][j] = new JTextArea(600/losningsforslag.length, 600/losningsforslag.length);
				ruter[i][j].setText(losningsforslag[i][j]);
				ruter[i][j].setFont(new Font("SansSerif", Font.BOLD, 32));
				ruter[i][j].setBorder(BorderFactory.createMatteBorder(topp, venstre, ned, hoyre, Color.black));
				
				panel.add(ruter[i][j]);
				repaint();
			}
		}
	} 

	/**
	*En indre klasse som har ansvar for å ta imot og tolke knappetrykk.
	*/
	class Lytter implements ActionListener {

		/**
		*Finner ut hvilken knapp som ble trykket.
		*@param e Et objekt av ActionEvent.
		*/
		public void actionPerformed (ActionEvent e) {
			if(e.getSource() == neste) {

				if (losningsteller == sudokubeholder.getBrettbeholderSize() || sudokubeholder.getBrettbeholderSize() == 1) {
					neste.setBackground(Color.LIGHT_GRAY);
				} else if(losningsteller < sudokubeholder.getBrettbeholderSize()-1 || sudokubeholder.getBrettbeholderSize() != 1) {
					panel.removeAll();
					panelMeny.remove(viserLosning);
					hentLosningsforslag();
					panel.revalidate();
					panelMeny.revalidate();
					panel.repaint();
					panelMeny.repaint();
					repaint();
				} 

			} else if (e.getSource() == velgFil) {
				losningsteller = 0;
				panel.removeAll();
				panelMeny.removeAll();
				panel.updateUI();
				panelMeny.updateUI();
				repaint();
				sudokubeholder.nullstillBrettbeholder();
				filvelger();
			}
		}
	}
}


import java.io.FileWriter;

/**
*Skriver de funnede løsningene til en gitt fil.
*/
public class UtskriftTilFil {
	String fil;
	Sudokubeholder sudokubeholder;

	UtskriftTilFil(String fil) {
		this.fil = fil;
	}

	/**
	*Setter sudokubeholder til å være samme beholder som i innlesning.
	*/
	public void setSudokubeholder(Sudokubeholder s) {
		sudokubeholder = s;
	}

	/**
	*Skriver løsninger til fil. 
	*/
	public void skrivTilFil() {
		FileWriter skriver;

		try{
			skriver = new FileWriter(fil);

			skriver.write(sudokubeholder.skrivAntallLosningerTilFil() + "\n");
			skriver.write(sudokubeholder.skrivTilFil());
			skriver.close();

			System.out.println("Sjekk filen " + fil + ".");

		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
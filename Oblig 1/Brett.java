/**
*Denne klassen modellerer et sudokubrett, og har en oversikt over hva det består av; kolonner, rader og bokser. 
*Den tar også midlertidig vare på løsningsforslag før de er ferdig utfylt.
*/

public class Brett {
	private Rute[][] rutebeholder; //Er der først og fremst med tanke på neste-pekere
	private Kolonne[] kolonnebeholder; //Inneholder kolonneobjekter som rutene peker på
	private Rad[] radbeholder; //Inneholder radobjekter som rutene peker på
	private Boks[] boksbeholder; //Inneholder boksobjekter som rutene peker på
	private Sudokubeholder sudokubeholder;
	/**
	*Konstruktøren initialiserer beholdere ved å bruke input fra bruker,
	*og lager en todimensjonal array som er like stor som sudokubrettet ved å
	*gange de oppgitte talene med hverandre.
	*@param loddrett Tar imot en høyde på brettet. 
	*@param vannrett Tar imot en bredde på brettet. 
	*/
	Brett(int loddrett, int vannrett, Sudokubeholder s) {
		int antallRuter = vannrett * loddrett;
		rutebeholder = new Rute[antallRuter][antallRuter];
		kolonnebeholder = new Kolonne[antallRuter];
		radbeholder = new Rad[antallRuter];
		boksbeholder = new Boks[antallRuter];
		sudokubeholder = s;
	}

	/**
	*@return Returnerer lengen til det todimensjonale arrayet rutebeholder.
	*/
	public int getRutebeholderLength() {
		return rutebeholder.length;
	}

	/**
	*Skriver ut innholdet til boks-, rad-, og kolonnebeholderne.
	*/
	public void skrivUtBeholderne() {
		System.out.println("");
		for(int i = 0; i < radbeholder.length; i++) {
			System.out.print("Kolonne: ");
			kolonnebeholder[i].skrivUtRuteoversikt();
			System.out.print("Rad: ");
			radbeholder[i].skrivUtRuteoversikt();
			System.out.print("Boks: ");
			boksbeholder[i].skrivUtRuteoversikt();
		}
	}

	/**
	*Henter ut en Kolonne fra en gitt plass i Kolonnebeholderen.
	*@param index Indeksen Kolonne skal hentes fra.
	*/
	public Kolonne getKolonne(int index) {
		return kolonnebeholder[index];
	}

	/**
	*Henter ut en Rute fra en gitt plass i Rutebeholderen.
	*@param i Raden Ruten skal hentes fra.
	*@param j Kolonnen Ruten skal hentes fra.
	*/
	public Rute getRute(int i, int j) {
		return rutebeholder[i][j];
	}

	/**
	*Setter variabelen neste i et ruteobjekt til å peke på det neste ruteobjektet i rutebeholder.
	*/
	public void settRuteNestepekere() {
		for(int i = 0; i < rutebeholder.length; i++) {
			for(int j = 0; j < rutebeholder.length; j++) {
				if(j != rutebeholder.length-1) {
					rutebeholder[i][j].neste = rutebeholder[i][j+1];
				} else if(i != rutebeholder.length-1) {
					rutebeholder[i][j].neste = rutebeholder[i+1][0];
				}	
			}
			System.out.println("");
		}
		action();
	}

	/**
	*Setter i gang utfylling av brettet.
	*/
	public void action() {
		rutebeholder[0][0].fyllUtRestenAvBrettet();
	}	

	/** 
	*Lagrer en tom rute uten verdi på første ledige plass i rutebeholderen.
	*@param rute Er et objekt av klassen Rute.
	*/
	public void settInnRute(Rute rute) {
		int temp = 0;
		for(int i = 0; i < rutebeholder.length; i++) {
			for(int j = 0; j < rutebeholder[i].length; j++) {

				if(rutebeholder[i][j] == null) {
					rutebeholder[i][j] = rute; 
					temp = j; break;
				}
			} 

			if (rutebeholder[i][temp] == rute) { break; }
		}
	}

	/**
	*En rutes verdi konverteres til en String og lagres oppi løsningforslaget.
	*@param rute Tar imot et objekt av klassen Rute.
	*/
	public void konverterBrettTilLosningsforslag() {
		String[][] losningsforslag = new String[rutebeholder.length][rutebeholder.length];

		String temp = "";

		for(int i = 0; i < losningsforslag.length; i++) {
			for(int j = 0; j < losningsforslag.length; j++) {
				if(losningsforslag[i][j] == null) {

					//Dersom tallet i ruta er større enn ni, sendes det til sudokubeholderens konverteringsmetode. 
					if(rutebeholder[i][j].getVerdi() > 9) {
						losningsforslag[i][j] = sudokubeholder.konverterTallTilBokstav(rutebeholder[i][j].getVerdi());
					} else {
						temp += rutebeholder[i][j].getVerdi();
						losningsforslag[i][j] = temp;
						temp = "";
					} 
				}
			} 
		} 

		sudokubeholder.settInn(losningsforslag);
	} 

	/**
	*Lagrer et objekt av klassen Boks på en gitt plass i boksbeholderen.
	*@param b Er et objekt av klassen Boks.
	*@param index Er et heltall, plassen boksen skal lagres på.
	*/
	public void lagreBoks(Boks b, int index) {
		boksbeholder[index] = b;
	}

	/**
	*Lagrer et objekt av klassen Boks i boksbeholderen.
	*@param b Er et objekt av klassen Boks.
	*/
	public void lagreBoks(Boks b) {
		for(int i = 0; i < boksbeholder.length; i++) {
			if(boksbeholder[i] == null) {
				boksbeholder[i] = b; break;
			}
		}
	}

	/**
	*Lagrer et objekt av klassen Rad i radbeholderen.
	*@param r Er et objekt av klassen Rad.
	*/
	public void lagreRad(Rad r) {
		for(int i = 0; i < radbeholder.length; i++) {
			if(radbeholder[i] == null) {
				radbeholder[i] = r; break;
			}
		}
	}

	/**
	*Lagrer et objekt av klassen Kolonne i kolonnebeholderen.
	*@param k Er et objekt av klassen Kolonne.
	*/
	public void lagreKolonne(Kolonne k) {
		for(int i = 0; i < kolonnebeholder.length; i++) {
			if(kolonnebeholder[i] == null) {
				kolonnebeholder[i] = k; break;
			}
		}
	}

	/**
	*Lagrer et objekt av klassen Rute på en gitt plass i kolonnebeholderen.
	*@param rute Er et objekt av klassen Rute.
	*@param i Er et heltall, indeksen Rute skal lagres på i kolonnebeholderen.
	*/
	public void lagreRuteIKolonnebeholder(Rute rute, int i) {
		kolonnebeholder[i].lagreRute(rute);
	}

	/**
	*Skriver ut det todimensjonale arrayet Rutebeholder.
	*/
	public void skrivUtRutebeholder() {
		for(int i = 0; i < rutebeholder.length; i++) {
			for(int j = 0; j < rutebeholder[i].length; j++) {
				if(rutebeholder[i][j] == null) continue;
				System.out.print(rutebeholder[i][j].getVerdi() + " ");
			} System.out.println("");
		} 
	}
}
/**
*INF1010 Obligatorisk oppgave 5
*@author Janna Åmodt (jannaa)
*/

import javax.swing.*;

public class Oblig5 {
	public static void main (String[] args) {

		final Innlesning innlesning;
		UtskriftTilFil utskrift = null;
		boolean skrivTilSkjerm = true;

		innlesning = new Innlesning(args[0]);

		/*Tester på om bruker har angitt flere enn en parameter, noe som vil 
		si at programmet skal klargjøre en utskrift til fil på slutten.*/
		if(args.length == 2) {
			utskrift = new UtskriftTilFil(args[1]);
			skrivTilSkjerm = false;
		}

		//Henter sudokubeholderen fra Innlesning og kopierer den til utskrift. 
		if(skrivTilSkjerm == false) {
			utskrift.setSudokubeholder(innlesning.getSudokubeholder());
			utskrift.skrivTilFil();
		}

		//GUI-relatert stuff som måtte være med. 
		SwingUtilities.invokeLater(
			new Runnable() {
				public void run() {
					new SudokuGUI(innlesning);
				}
			}); //end GUI-relatert stuff
	}
}
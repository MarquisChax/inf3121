/**
*Grensesnittet sikrer at klassen Brettdeler, som er superklasse for Boks, Rad og Kolonne, implementerer 
*metoden finnesTallet(int verdi) slik at programmet skal kunne finne et passende tall basert på tall som
*er lagret i ruteoversikt-arrayet.
*/
public interface Bruteforce {

	/**
	*Metoden sørger for å finne ut om tallet finnes i arrayet fra før av.
	*@param verdi Er et heltall som det skal søkes etter i arrayet.
	*@return Returnerer true hvis tallet er funnet, og gir dermed beskjed om at det ikke passer.
	*/
	public boolean finnesTallet(int verdi);
}
